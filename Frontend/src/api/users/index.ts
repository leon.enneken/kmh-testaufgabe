import axios, { AxiosResponse } from 'axios';
import { UserBody, UserResponse } from '../@types/user';

const request = axios.create({
  baseURL: '/api/user'
});

export default {
  
  getAllUsers(): Promise<AxiosResponse<UserResponse[], UserResponse[]>> {
    return request.get('/');
  },

  createUser(body: UserBody): Promise<AxiosResponse<UserResponse, UserResponse>> {
    return request.post('/', body);
  },

  getUser(id: string): Promise<AxiosResponse<UserResponse, UserResponse>> {
    return request.get(`/${id}`);
  },

  patchUser(id: string, body: UserBody): Promise<AxiosResponse<UserResponse, UserResponse>> {
    return request.patch(`/${id}`, body);
  },

  deleteUser(id: string): Promise<AxiosResponse<UserResponse, UserResponse>> {
    return request.delete(`/${id}`);
  }
}

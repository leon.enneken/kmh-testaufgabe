import roleRights from './role-rights';
import roles from './roles';
import statistics from './statistics';
import users from './users';

export * from './@types';

export const api = {
  roleRights,
  roles,
  statistics,
  users
}
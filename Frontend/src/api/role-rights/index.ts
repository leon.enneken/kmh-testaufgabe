import axios, { AxiosResponse } from 'axios';
import { RoleRightBody, RoleRightResponse } from '../@types/role-right';

const request = axios.create({
  baseURL: '/api/role-right'
});

export default {

  getAllRoleRights(): Promise<AxiosResponse<RoleRightResponse[], RoleRightResponse[]>> {
    return request.get('/');
  },

  createRoleRight(body: RoleRightBody): Promise<AxiosResponse<RoleRightResponse, RoleRightResponse>> {
    return request.post('/', body);
  },

  getRoleRight(id: string): Promise<AxiosResponse<RoleRightResponse, RoleRightResponse>> {
    return request.get(`/${id}`);
  },

  patchRoleRight(id: string, body: RoleRightBody): Promise<AxiosResponse<RoleRightResponse, RoleRightResponse>> {
    return request.patch(`/${id}`, body);
  },

  deleteRoleRight(id: string): Promise<AxiosResponse<RoleRightResponse, RoleRightResponse>> {
    return request.delete(`/${id}`);
  }
}

import axios, { AxiosResponse } from 'axios';
import { CountResponse } from '../@types';

const request = axios.create({
  baseURL: '/api/statistics'
});

export default {

  getCounts(): Promise<AxiosResponse<CountResponse, CountResponse>> {
    return request.get('/counts');
  }
}


export type UserRole = {
  id: string;
  name: string;
}

export type UserResponse = {
  id: string;
  firstName: string;
  lastName: string;
  email: string;
  phoneNumber?: string;
  roleIds: string[];
  roles: UserRole[];
  updatedAt: Date;
  createdAt: Date;
}

export type UserBody = {
  firstName: string;
  lastName: string;
  email: string;
  phoneNumber?: string;
  roleIds: string[];
}
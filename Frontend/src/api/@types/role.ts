export type RoleCount = {
  rights: number;
  users: number;
}

export type RoleResponse = {
  id: string;
  name: string;
  rightIds: string[];
  updatedAt: Date;
  createdAt: Date;
  _count: RoleCount;
}

export type RoleBody = {
  name: string;
  rightIds?: string[];
}
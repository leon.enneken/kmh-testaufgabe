export type RoleRightCount = {
  roles: number;
}

export type RoleRightResponse = {
  id: string;
  name: string;
  updatedAt: Date;
  createdAt: Date;
  _count: RoleRightCount;
}

export type RoleRightBody = {
  name: string;
}
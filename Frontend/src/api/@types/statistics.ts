export type CountResponse = {
  roleCount: number;
  roleRightCount: number;
  userCount: number;
}
import axios, { AxiosResponse } from 'axios';
import { RoleBody, RoleResponse } from '../@types/role';

const request = axios.create({
  baseURL: '/api/role'
});

export default {

  getAllRoles(): Promise<AxiosResponse<RoleResponse[], RoleResponse[]>> {
    return request.get('/');
  },

  createRole(body: RoleBody): Promise<AxiosResponse<RoleResponse, RoleResponse>> {
    return request.post('/', body);
  },

  getRole(id: string): Promise<AxiosResponse<RoleResponse, RoleResponse>> {
    return request.get(`/${id}`);
  },

  patchRole(id: string, body: RoleBody): Promise<AxiosResponse<RoleResponse, RoleResponse>> {
    return request.patch(`/${id}`, body);
  },

  deleteRole(id: string): Promise<AxiosResponse<RoleResponse, RoleResponse>> {
    return request.delete(`/${id}`);
  }
}

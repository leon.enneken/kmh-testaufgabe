import { ApiProperty } from '@nestjs/swagger';

export class RoleCount {
  
  @ApiProperty({
    type: 'Integer',
    format: 'int32',
    readOnly: true,
    required: true,
    description: 'Number of role rights that use this role'
  })
  rights: number;
  
  @ApiProperty({
    type: 'Integer',
    format: 'int32',
    readOnly: true,
    required: true,
    description: 'Number of users who have this role'
  })
  users: number;
}

export class RoleResponse {

  @ApiProperty({
    type: String,
    readOnly: true,
    required: true,
    description: 'Role object id'
  })
  id: string;

  @ApiProperty({
    type: String,
    required: true,
    description: 'Name of the role'
  })
  name: string;

  @ApiProperty({
    type: String,
    isArray: true,
    required: true,
    description: 'Role right ids that use this role'
  })
  rightIds: string[];

  @ApiProperty({
    type: Date,
    readOnly: true,
    required: true,
    description: 'Last date of changes'
  })
  updatedAt: Date;

  @ApiProperty({
    type: Date,
    readOnly: true,
    required: true,
    description: 'Creation date of the role'
  })
  createdAt: Date;

  @ApiProperty({
    type: RoleCount,
    required: true,
    description: 'Counts of related objects of the role'
  })
  _count: RoleCount;
}

export class RoleBody {

  @ApiProperty({
    type: String,
    required: true,
    description: 'Name of the role'
  })
  name: string;

  @ApiProperty({
    type: String,
    isArray: true,
    required: true,
    description: 'Role right ids that use this role'
  })
  rightIds: string[];
}
import { Body, Controller, Delete, Get, Param, Patch, Post } from '@nestjs/common';
import { ApiConflictResponse, ApiConsumes, ApiCreatedResponse, ApiNotFoundResponse, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { RoleBody, RoleResponse } from './role.entity';
import { RoleService } from './role.service';

@ApiTags('Role')
@Controller('role')
export class RoleController {

  constructor(private readonly _service: RoleService) {

  }

  @Get()
  @ApiOperation({
    operationId: 'getAllRoles',
    summary: 'Returns an array of roles',
    description: 'Returns an array of roles'
  })
  @ApiOkResponse({
    type: RoleResponse,
    isArray: true
  })
  async getAllRoles(): Promise<RoleResponse[]> {
    return this._service.getAllRoles();
  }

  @Post()
  @ApiOperation({
    operationId: 'createRole',
    summary: 'Create a new role object',
    description: 'Create a new role object'
  })
  @ApiCreatedResponse({
    type: RoleResponse
  })
  @ApiConflictResponse({
    description: 'Role already exists!'
  })
  @ApiConsumes('application/x-www-form-urlencoded', 'application/json')
  async createRole(@Body() body: RoleBody): Promise<RoleResponse> {
    return this._service.createRole(body);
  }

  @Get(':id')
  @ApiOperation({
    operationId: 'getRole',
    summary: 'Returns a single role object',
    description: 'Returns a single role object'
  })
  @ApiOkResponse({
    type: RoleResponse
  })
  @ApiNotFoundResponse({
    description: 'Role not found!'
  })
  async getRole(@Param('id') id: string): Promise<RoleResponse> {
    return this._service.getRole(id);
  }

  @Patch(':id')
  @ApiOperation({
    operationId: 'patchRole',
    summary: 'Patch a existing role object',
    description: 'Patch a existing role object'
  })
  @ApiOkResponse({
    type: RoleResponse
  })
  @ApiConflictResponse({
    description: 'Role already exists!'
  })
  @ApiNotFoundResponse({
    description: 'Role not found!'
  })
  @ApiConsumes('application/x-www-form-urlencoded', 'application/json')
  async patchRole(@Param('id') id: string, @Body() body: RoleBody): Promise<RoleResponse> {
    return this._service.patchRole(id, body);
  }

  @Delete(':id')
  @ApiOperation({
    operationId: 'deleteRole',
    summary: 'Delete a single role object and returns it',
    description: 'Delete a single role object and returns it'
  })
  @ApiOkResponse({
    type: RoleResponse
  })
  @ApiNotFoundResponse({
    description: 'Role not found!'
  })
  async deleteRole(@Param('id') id: string): Promise<RoleResponse> {
    return this._service.deleteRole(id);
  }

}

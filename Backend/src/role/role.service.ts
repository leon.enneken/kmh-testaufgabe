import { ConflictException, Injectable, NotFoundException } from '@nestjs/common';
import { Prisma, RoleRight } from '@prisma/client';
import { PrismaService } from 'src/@services';
import { RoleBody, RoleResponse } from './role.entity';

@Injectable()
export class RoleService {

  constructor(private readonly _prisma: PrismaService) {

  }

  async getAllRoles(): Promise<RoleResponse[]> {
    return this._prisma.role.findMany({
      include: {
        _count: {
          select: {
            rights: true,
            users: true
          }
        }
      },
      orderBy: {
        name: 'asc'
      }
    });
  }

  async createRole(body: RoleBody): Promise<RoleResponse> {
    const role = await this._prisma.role.findFirst({
      where: {
        name: {
          equals: body.name,
          mode: 'insensitive'
        }
      }
    });

    if (role) {
      throw new ConflictException('Role already exists!');
    }
    let roleRights: RoleRight[] = [];

    if(body.rightIds?.length !== 0) {
      roleRights = await this._prisma.roleRight.findMany({
        where: {
          id: {
            in: [...new Set(body.rightIds)]
          }
        }
      });
    }

    const data: Prisma.RoleCreateInput = {
      name: body.name
    };

    if(roleRights.length !== 0) {
      data.rights = {
        connect: roleRights.map((roleRight) => ({ id: roleRight.id }))
      };
    }

    return this._prisma.role.create({
      include: {
        _count: {
          select: {
            rights: true,
            users: true
          }
        }
      },
      data: data
    });
  }

  async getRole(id: string): Promise<RoleResponse> {
    const role = await this._prisma.role.findUnique({
      include: {
        _count: {
          select: {
            rights: true,
            users: true
          }
        }
      },
      where: {
        id: id
      }
    });

    if (!(role)) {
      throw new NotFoundException('Role not found!');
    }
    return role;
  }

  async patchRole(id: string, body: RoleBody): Promise<RoleResponse> {
    const role = await this._prisma.role.findUnique({
      where: {
        id: id
      }
    });

    if (!(role)) {
      throw new NotFoundException('Role not found!');
    }

    if (body.name.toLowerCase() !== role.name.toLowerCase()) {
      const lookupRole = await this._prisma.role.findFirst({
        where: {
          name: {
            equals: body.name,
            mode: 'insensitive'
          }
        }
      });

      if (lookupRole) {
        throw new ConflictException('Role already exists!');
      }
    }
    let roleRightIds: string[] = [];

    if(body.rightIds?.length !== 0) {
      const roleRights = await this._prisma.roleRight.findMany({
        where: {
          id: {
            in: [...new Set(body.rightIds)]
          }
        }
      });
      roleRightIds = roleRights.map((roleRight) => roleRight.id);
    }

    return this._prisma.role.update({
      include: {
        _count: {
          select: {
            rights: true,
            users: true
          }
        }
      },
      data: {
        name: body.name,
        rights: {
          disconnect: role.rightIds.filter((rightId) => !(roleRightIds.includes(rightId))).map((roleRight) => ({ id: roleRight })),
          connect: roleRightIds.map((roleRight) => ({ id: roleRight }))
        }
      },
      where: {
        id: role.id
      }
    });
  }

  async deleteRole(id: string): Promise<RoleResponse> {
    const role = await this._prisma.role.findUnique({
      where: {
        id: id
      }
    });

    if (!(role)) {
      throw new NotFoundException('Role not found!');
    }

    const roleRights = await this._prisma.roleRight.findMany({
      where: {
        roleIds: {
          has: role.id
        }
      }
    });

    for (const roleRight of roleRights) {
      const updatedRoleIds = roleRight.roleIds.filter((roleId) => role.id !== roleId);

      await this._prisma.roleRight.update({
        data: {
          roleIds: {
            set: updatedRoleIds
          }
        },
        where: {
          id: roleRight.id
        }
      });
    }

    const users = await this._prisma.user.findMany({
      where: {
        roleIds: {
          has: role.id
        }
      }
    });

    for (const user of users) {
      const updatedRoleIds = user.roleIds.filter((roleId) => role.id !== roleId);

      await this._prisma.user.update({
        data: {
          roleIds: {
            set: updatedRoleIds
          }
        },
        where: {
          id: user.id
        }
      });
    }

    return this._prisma.role.delete({
      include: {
        _count: {
          select: {
            rights: true,
            users: true
          }
        }
      },
      where: {
        id: role.id
      }
    });
  }
}

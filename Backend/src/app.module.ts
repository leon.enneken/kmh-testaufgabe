import { Module } from '@nestjs/common';
import { RoleRightModule } from './role-right/role-right.module';
import { RoleModule } from './role/role.module';
import { StatisticsModule } from './statistics/statistics.module';
import { UserModule } from './user/user.module';

@Module({
  imports: [
    RoleModule,
    RoleRightModule,
    StatisticsModule,
    UserModule
  ],
  controllers: [],
  providers: [],
})
export class AppModule { }

import { ApiProperty } from '@nestjs/swagger';

export class CountResponse {

  @ApiProperty({
    type: 'Integer',
    format: 'int32',
    readOnly: true,
    required: true,
    description: 'Count of role objects'
  })
  roleCount: number;

  @ApiProperty({
    type: 'Integer',
    format: 'int32',
    readOnly: true,
    required: true,
    description: 'Count of role right objects'
  })
  roleRightCount: number;

  @ApiProperty({
    type: 'Integer',
    format: 'int32',
    readOnly: true,
    required: true,
    description: 'Count of user objects'
  })
  userCount: number;
}
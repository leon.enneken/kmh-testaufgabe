import { Injectable } from '@nestjs/common';
import { PrismaService } from 'src/@services';
import { CountResponse } from './statistics.entity';

@Injectable()
export class StatisticsService {

  constructor(private readonly _prisma: PrismaService) {

  }

  async getCounts(): Promise<CountResponse> {
    const roleCount = await this._prisma.role.count();
    const roleRightCount = await this._prisma.roleRight.count();
    const userCount = await this._prisma.user.count();

    return {
      roleCount,
      roleRightCount,
      userCount,
    };
  }
}

import { Controller, Get } from '@nestjs/common';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { CountResponse } from './statistics.entity';
import { StatisticsService } from './statistics.service';

@ApiTags('Statistics')
@Controller('statistics')
export class StatisticsController {

  constructor(private readonly _service: StatisticsService) {

  }

  @Get('counts')
  @ApiOperation({
    operationId: 'getCounts',
    summary: 'Returns all collection counts',
    description: 'Returns all collection counts'
  })
  @ApiOkResponse({
    type: CountResponse
  })
  async getCounts(): Promise<CountResponse> {
    return this._service.getCounts();
  }

}

import { ApiProperty } from '@nestjs/swagger';

export class UserRole {

  @ApiProperty({
    type: String,
    readOnly: true,
    required: true,
    description: 'Role object id'
  })
  id: string;

  @ApiProperty({
    type: String,
    required: true,
    description: 'Name of the role'
  })
  name: string;
}

export class UserResponse {

  @ApiProperty({
    type: String,
    readOnly: true,
    required: true,
    description: 'User object id'
  })
  id: string;

  @ApiProperty({
    type: String,
    required: true,
    description: 'First name of the user'
  })
  firstName: string;

  @ApiProperty({
    type: String,
    required: true,
    default: 'Last name of the user'
  })
  lastName: string;

  @ApiProperty({
    type: String,
    required: true,
    description: 'Email address of the user'
  })
  email: string;

  @ApiProperty({
    type: String,
    required: false,
    description: 'Phone number of the user (Optional)'
  })
  phoneNumber?: string;

  @ApiProperty({
    type: String,
    isArray: true,
    required: true,
    description: 'Role ids that use this user'
  })
  roleIds: string[];

  @ApiProperty({
    type: UserRole,
    isArray: true,
    required: true,
    description: 'Roles that use this user'
  })
  roles: UserRole[];

  @ApiProperty({
    type: Date,
    readOnly: true,
    required: true,
    description: 'Last date of changes'
  })
  updatedAt: Date;

  @ApiProperty({
    type: Date,
    readOnly: true,
    required: true,
    description: 'Creation date of the user'
  })
  createdAt: Date;
}

export class UserBody {

  @ApiProperty({
    type: String,
    required: true,
    description: 'First name of the user'
  })
  firstName: string;

  @ApiProperty({
    type: String,
    required: true,
    default: 'Last name of the user'
  })
  lastName: string;

  @ApiProperty({
    type: String,
    required: true,
    description: 'Email address of the user'
  })
  email: string;

  @ApiProperty({
    type: String,
    required: false,
    description: 'Phone number of the user (Optional)'
  })
  phoneNumber?: string;

  @ApiProperty({
    type: String,
    isArray: true,
    required: true,
    description: 'Role ids that use this user'
  })
  roleIds: string[];
}
import { Body, Controller, Delete, Get, Param, Patch, Post } from '@nestjs/common';
import { ApiConflictResponse, ApiConsumes, ApiCreatedResponse, ApiNotFoundResponse, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { UserBody, UserResponse } from './user.entity';
import { UserService } from './user.service';

@ApiTags('User')
@Controller('user')
export class UserController {

  constructor(private readonly _service: UserService) {

  }

  @Get()
  @ApiOperation({
    operationId: 'getAllUsers',
    summary: 'Returns an array of users',
    description: 'Returns an array of users'
  })
  @ApiOkResponse({
    type: UserResponse,
    isArray: true
  })
  async getAllUsers(): Promise<UserResponse[]> {
    return this._service.getAllUsers();
  }

  @Post()
  @ApiOperation({
    operationId: 'createUser',
    summary: 'Create a new user object',
    description: 'Create a new user object'
  })
  @ApiCreatedResponse({
    type: UserResponse
  })
  @ApiConflictResponse({
    description: 'Email address already exists!'
  })
  @ApiConsumes('application/x-www-form-urlencoded', 'application/json')
  async createUser(@Body() body: UserBody): Promise<UserResponse> {
    return this._service.createUser(body);
  }

  @Get(':id')
  @ApiOperation({
    operationId: 'getUser',
    summary: 'Returns a single user object',
    description: 'Returns a single user object'
  })
  @ApiOkResponse({
    type: UserResponse
  })
  @ApiNotFoundResponse({
    description: 'User not found!'
  })
  async getUser(@Param('id') id: string): Promise<UserResponse> {
    return this._service.getUser(id);
  }

  @Patch(':id')
  @ApiOperation({
    operationId: 'patchUser',
    summary: 'Patch a existing user object',
    description: 'Patch a existing user object'
  })
  @ApiOkResponse({
    type: UserResponse
  })
  @ApiConflictResponse({
    description: 'Email address already exists!'
  })
  @ApiNotFoundResponse({
    description: 'User not found!'
  })
  @ApiConsumes('application/x-www-form-urlencoded', 'application/json')
  async patchUser(@Param('id') id: string, @Body() body: UserBody): Promise<UserResponse> {
    return this._service.patchUser(id, body);
  }

  @Delete(':id')
  @ApiOperation({
    operationId: 'deleteUser',
    summary: 'Delete a single user object and returns it',
    description: 'Delete a single user object and returns it'
  })
  @ApiOkResponse({
    type: UserResponse
  })
  @ApiNotFoundResponse({
    description: 'User not found!'
  })
  async deleteUser(@Param('id') id: string): Promise<UserResponse> {
    return this._service.deleteUser(id);
  }

}

import { ConflictException, Injectable, NotFoundException } from '@nestjs/common';
import { Prisma, Role } from '@prisma/client';
import { PrismaService } from 'src/@services';
import { UserBody, UserResponse } from './user.entity';

@Injectable()
export class UserService {

  constructor(private readonly _prisma: PrismaService) {

  }

  async getAllUsers(): Promise<UserResponse[]> {
    return this._prisma.user.findMany({
      include: {
        roles: {
          select: {
            id: true,
            name: true
          }
        }
      },
      orderBy: {
        lastName: 'asc'
      }
    });
  }

  async createUser(body: UserBody): Promise<UserResponse> {
    const user = await this._prisma.user.findFirst({
      where: {
        email: {
          equals: body.email,
          mode: 'insensitive'
        }
      }
    });

    if (user) {
      throw new ConflictException('Email address already exists!');
    }
    let roles: Role[] = [];

    if(body.roleIds?.length !== 0) {
      roles = await this._prisma.role.findMany({
        where: {
          id: {
            in: [...new Set(body.roleIds)]
          }
        }
      });
    }

    const data: Prisma.UserCreateInput = {
      firstName: body.firstName,
      lastName: body.lastName,
      email: body.email
    };

    if (body.phoneNumber) {
      data.phoneNumber = body.phoneNumber;
    }

    if(roles.length !== 0) {
      data.roles = {
        connect: roles.map((role) => ({ id: role.id }))
      };
    }

    return this._prisma.user.create({
      include: {
        roles: {
          select: {
            id: true,
            name: true
          }
        }
      },
      data: data
    });
  }

  async getUser(id: string): Promise<UserResponse> {
    const user = await this._prisma.user.findUnique({
      include: {
        roles: {
          select: {
            id: true,
            name: true
          }
        }
      },
      where: {
        id: id
      }
    });

    if (!(user)) {
      throw new NotFoundException('User not found!');
    }
    return user;
  }

  async patchUser(id: string, body: UserBody): Promise<UserResponse> {
    const user = await this._prisma.user.findUnique({
      where: {
        id: id
      }
    });

    if (!(user)) {
      throw new NotFoundException('User not found!');
    }

    if (body.email.toLowerCase() !== user.email.toLowerCase()) {
      const lookupUser = await this._prisma.user.findFirst({
        where: {
          email: {
            equals: body.email,
            mode: 'insensitive'
          }
        }
      });

      if (lookupUser) {
        throw new ConflictException('Email address already exists!');
      }
    }

    const data: Prisma.UserUpdateInput = {};

    if (body.firstName !== user.firstName) {
      data.firstName = body.firstName;
    }

    if (body.lastName !== user.lastName) {
      data.lastName = body.lastName;
    }

    if (body.email !== user.email) {
      data.email = body.email;
    }

    if (body.phoneNumber !== user.phoneNumber) {
      data.phoneNumber = (user.phoneNumber ? user.phoneNumber : {
        unset: true
      });
    }
    let roleIds: string[] = [];

    if(body.roleIds?.length !== 0) {
      const roles = await this._prisma.role.findMany({
        where: {
          id: {
            in: [...new Set(body.roleIds)]
          }
        }
      });
      roleIds = roles.map((role) => role.id);
    }

    data.roles = {
      disconnect: user.roleIds.filter((roleId) => !(roleIds.includes(roleId))).map((roleId) => ({ id: roleId })),
      connect: roleIds.map((roleId) => ({ id: roleId }))
    };

    return this._prisma.user.update({
      include: {
        roles: {
          select: {
            id: true,
            name: true
          }
        }
      },
      data: data,
      where: {
        id: user.id
      }
    });
  }

  async deleteUser(id: string): Promise<UserResponse> {
    const user = await this._prisma.user.findUnique({
      where: {
        id: id
      }
    });

    if (!(user)) {
      throw new NotFoundException('User not found!');
    }

    const roles = await this._prisma.role.findMany({
      where: {
        userIds: {
          has: user.id
        }
      }
    });

    for (const role of roles) {
      const updatedUserIds = role.userIds.filter((userId) => user.id !== userId);

      await this._prisma.role.update({
        data: {
          userIds: {
            set: updatedUserIds
          }
        },
        where: {
          id: role.id
        }
      });
    }

    return this._prisma.user.delete({
      include: {
        roles: {
          select: {
            id: true,
            name: true
          }
        }
      },
      where: {
        id: user.id
      }
    });
  }
}

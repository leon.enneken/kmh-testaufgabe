import { ApiProperty } from '@nestjs/swagger';

export class RoleRightCount {
  
  @ApiProperty({
    type: 'Integer',
    format: 'int32',
    readOnly: true,
    required: true,
    description: 'Roles count that use this role right'
  })
  roles: number;
}

export class RoleRightResponse {

  @ApiProperty({
    type: String,
    readOnly: true,
    required: true,
    description: 'Role right object id'
  })
  id: string;

  @ApiProperty({
    type: String,
    required: true,
    description: 'Name of the role right'
  })
  name: string;

  @ApiProperty({
    type: Date,
    readOnly: true,
    required: true,
    description: 'Last date of changes'
  })
  updatedAt: Date;

  @ApiProperty({
    type: Date,
    readOnly: true,
    required: true,
    description: 'Creation date of the role right'
  })
  createdAt: Date;

  @ApiProperty({
    type: RoleRightCount,
    required: true,
    description: 'Counts of related objects of the role right'
  })
  _count: RoleRightCount;
}

export class RoleRightBody {

  @ApiProperty({
    type: String,
    required: true,
    description: 'Name of the role right'
  })
  name: string;
}
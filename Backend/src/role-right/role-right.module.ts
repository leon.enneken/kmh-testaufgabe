import { Module } from '@nestjs/common';
import { PrismaModule } from 'src/@services';
import { RoleRightController } from './role-right.controller';
import { RoleRightService } from './role-right.service';

@Module({
  imports: [PrismaModule],
  controllers: [RoleRightController],
  providers: [RoleRightService],
})
export class RoleRightModule { }
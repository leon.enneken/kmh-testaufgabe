import { Body, Controller, Delete, Get, Param, Patch, Post } from '@nestjs/common';
import { ApiConflictResponse, ApiConsumes, ApiCreatedResponse, ApiNotFoundResponse, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { RoleRightBody, RoleRightResponse } from './role-right.entity';
import { RoleRightService } from './role-right.service';

@ApiTags('Role-Right')
@Controller('role-right')
export class RoleRightController {

  constructor(private readonly _service: RoleRightService) {

  }

  @Get()
  @ApiOperation({
    operationId: 'getAllRoleRights',
    summary: 'Returns an array of role rights',
    description: 'Returns an array of role rights'
  })
  @ApiOkResponse({
    type: RoleRightResponse,
    isArray: true
  })
  async getAllRoleRights(): Promise<RoleRightResponse[]> {
    return this._service.getAllRoleRights();
  }

  @Post()
  @ApiOperation({
    operationId: 'createRoleRight',
    summary: 'Create a new role right object',
    description: 'Create a new role right object'
  })
  @ApiCreatedResponse({
    type: RoleRightResponse
  })
  @ApiConflictResponse({
    description: 'Role right already exists!'
  })
  @ApiConsumes('application/x-www-form-urlencoded', 'application/json')
  async createRoleRight(@Body() body: RoleRightBody): Promise<RoleRightResponse> {
    return this._service.createRoleRight(body);
  }

  @Get(':id')
  @ApiOperation({
    operationId: 'getRoleRight',
    summary: 'Returns a single role right object',
    description: 'Returns a single role right object'
  })
  @ApiOkResponse({
    type: RoleRightResponse
  })
  @ApiNotFoundResponse({
    description: 'Role right not found!'
  })
  async getRoleRight(@Param('id') id: string): Promise<RoleRightResponse> {
    return this._service.getRoleRight(id);
  }

  @Patch(':id')
  @ApiOperation({
    operationId: 'patchRoleRight',
    summary: 'Patch a existing role right object',
    description: 'Patch a existing role right object'
  })
  @ApiOkResponse({
    type: RoleRightResponse
  })
  @ApiConflictResponse({
    description: 'Role right already exists!'
  })
  @ApiNotFoundResponse({
    description: 'Role right not found!'
  })
  @ApiConsumes('application/x-www-form-urlencoded', 'application/json')
  async patchRoleRight(@Param('id') id: string, @Body() body: RoleRightBody): Promise<RoleRightResponse> {
    return this._service.patchRoleRight(id, body);
  }

  @Delete(':id')
  @ApiOperation({
    operationId: 'deleteRoleRight',
    summary: 'Delete a single role right object and returns it',
    description: 'Delete a single role right object and returns it'
  })
  @ApiOkResponse({
    type: RoleRightResponse
  })
  @ApiNotFoundResponse({
    description: 'Role right not found!'
  })
  async deleteRoleRight(@Param('id') id: string): Promise<RoleRightResponse> {
    return this._service.deleteRoleRight(id);
  }

}

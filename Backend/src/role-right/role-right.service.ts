import { ConflictException, Injectable, NotFoundException } from '@nestjs/common';
import { PrismaService } from 'src/@services';
import { RoleRightBody, RoleRightResponse } from './role-right.entity';

@Injectable()
export class RoleRightService {

  constructor(private readonly _prisma: PrismaService) {

  }

  async getAllRoleRights(): Promise<RoleRightResponse[]> {
    return this._prisma.roleRight.findMany({
      select: {
        id: true,
        name: true,
        roleIds: false,
        updatedAt: true,
        createdAt: true,
        _count: {
          select: {
            roles: true
          }
        }
      },
      orderBy: {
        name: 'asc'
      }
    });
  }

  async createRoleRight(body: RoleRightBody): Promise<RoleRightResponse> {
    const roleRight = await this._prisma.roleRight.findFirst({
      where: {
        name: {
          equals: body.name,
          mode: 'insensitive'
        }
      }
    });

    if (roleRight) {
      throw new ConflictException('Role right already exists!');
    }

    return this._prisma.roleRight.create({
      select: {
        id: true,
        name: true,
        roleIds: false,
        updatedAt: true,
        createdAt: true,
        _count: {
          select: {
            roles: true
          }
        }
      },
      data: {
        name: body.name
      }
    });
  }

  async getRoleRight(id: string): Promise<RoleRightResponse> {
    const roleRight = await this._prisma.roleRight.findUnique({
      select: {
        id: true,
        name: true,
        roleIds: false,
        updatedAt: true,
        createdAt: true,
        _count: {
          select: {
            roles: true
          }
        }
      },
      where: {
        id: id
      }
    });

    if (!(roleRight)) {
      throw new NotFoundException('Role right not found!');
    }
    return roleRight;
  }

  async patchRoleRight(id: string, body: RoleRightBody): Promise<RoleRightResponse> {
    const roleRight = await this._prisma.roleRight.findUnique({
      where: {
        id: id
      }
    });

    if (!(roleRight)) {
      throw new NotFoundException('Role right not found!');
    }

    if (body.name.toLowerCase() !== roleRight.name.toLowerCase()) {
      const lookupRoleRight = await this._prisma.roleRight.findFirst({
        where: {
          name: {
            equals: body.name,
            mode: 'insensitive'
          }
        }
      });

      if (lookupRoleRight) {
        throw new ConflictException('Role right already exists!');
      }
    }

    return this._prisma.roleRight.update({
      select: {
        id: true,
        name: true,
        roleIds: false,
        updatedAt: true,
        createdAt: true,
        _count: {
          select: {
            roles: true
          }
        }
      },
      data: {
        name: body.name
      },
      where: {
        id: roleRight.id
      }
    });
  }

  async deleteRoleRight(id: string): Promise<RoleRightResponse> {
    const roleRight = await this._prisma.roleRight.findUnique({
      where: {
        id: id
      }
    });

    if (!(roleRight)) {
      throw new NotFoundException('Role right not found!');
    }

    const roles = await this._prisma.role.findMany({
      where: {
        rightIds: {
          has: roleRight.id
        }
      }
    });

    for (const role of roles) {
      const updatedRightIds = role.rightIds.filter((rightId) => roleRight.id !== rightId);

      await this._prisma.role.update({
        data: {
          rightIds: {
            set: updatedRightIds
          }
        },
        where: {
          id: role.id
        }
      });
    }

    return this._prisma.roleRight.delete({
      select: {
        id: true,
        name: true,
        roleIds: false,
        updatedAt: true,
        createdAt: true,
        _count: {
          select: {
            roles: true
          }
        }
      },
      where: {
        id: roleRight.id
      }
    });
  }
}

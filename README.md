# KM/H Fullstack Test Aufgabe (Leon Enneken)

```
Dieses Projekt ist in 2 Teile aufgesplittet. Das Backend ist mit NestJS, 
OpenAPI und Prisma.io entwickelt worden. Als Datenbank wird hier auf 
MongoDB gesetzt.

Das Frontend ist mit Vue 3 und VuetifyJS entwickelt.
```

## Live Preview
```
Um dieses Projekt einfacher zu testen, habe ich eine Live Preview bereit 
gestellt. Welche unter folgenden Links erreichbar ist.
```
- Frontend / Dashboard:
  - https://kmh.leonenneken.de/
- Backend / Docs: 
  - https://kmh.leonenneken.de/api/docs

## Erklärung des Frontends / Dashboard

### Startseite / Dashboard
Kleine Übersicht wie viele Benutzer, Rollen und Berechtigungen existieren.

![Dashboard](https://images.es-intern.de/RXH2c4SZ.png)

### Rollen Rechte verwalten

Übersicht der Rollen Rechte | Hinzufügen / Bearbeiten Dialog
--- | ---
![Übersicht](https://images.es-intern.de/HnasD9jt.png)  | ![Hinzufügen/Bearbeiten](https://images.es-intern.de/hHNUoeoA.png)

### Rollen verwalten

Übersicht der Rollen | Hinzufügen / Bearbeiten Dialog
--- | ---
![Übersicht](https://images.es-intern.de/QZ3wzHrK.png)  | ![Hinzufügen/Bearbeiten](https://images.es-intern.de/Ed9G8nXk.png)

### Benutzer verwalten

Übersicht der Benutzer | Hinzufügen / Bearbeiten Dialog
--- | ---
![Übersicht](https://images.es-intern.de/8a8DOg5X.png)  | ![Hinzufügen/Bearbeiten](https://images.es-intern.de/3nxh96jz.png)


## Backend
```
Um das Backend zu starten, muss zuerst ein MongoDB Server existieren, 
welcher Replica Sets beinhaltet, da Prisma.io dies als Requirement hat.
```

### Installation
```sh
# In den Ordner navigieren
cd Backend

# Als nächstes muss die Datenbank URL in eine 
# ".env" Datei hinterlegt werden, als Example 
# gibt es hier die ".env-example"
cp .env-example .env && nano .env

# Dependencies installieren
npm install

# Prisma.io Schema generieren
npx prisma generate
```

### Backend starten
```sh
# Im Development Mode
npm run start:dev

# Bauen und Live Mode
npm run build && npm run start
```

- Nachdem das Backend gestartet ist, ist dies unter http://localhost:3000 erreichbar
- Ebenfalls gibt es eine OpenAPI Docs unter http://localhost:3000/docs

## Frontend
```
Um das Frontend einfach zu starten, empfehle ich dies im Development 
Mode zu starten, da dies automatisch einen Proxy Link zum Backend, über 
die "vite.config.mts" bereitstellt.

Dies routet das Backend direkt unter "http://localhost:5173/api"
```

### Installation & starten
```sh
# In den Ordner navigieren
cd Frontend

# Dependencies installieren
npm install

# Starten im Development Mode
npm run dev
```

- Sollte das Frontend im Development Mode gestartet werden, ist dies unter http://localhost:5173 erreichbar
- Das Backend wird automatisch auf http://localhost:5173/api geroutet

### Frontend bauen
```sh
npm run build
```